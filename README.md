Pacific Audiology Clinic is a women-owned and operated clinic, and we are dedicated to your success in achieving better hearing. We pride ourselves on offering high-quality assessments and various treatment options for all our patient’s needs.

Address: 5331 SW Macadam Ave, #395, Portland, OR 97239, USA

Phone: 503-719-4208
